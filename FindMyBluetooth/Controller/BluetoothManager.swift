//
//  BluetoothManager.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit
import CoreBluetooth

class BluetoothManager: NSObject {

    //CBCentralManager 管理整體藍牙狀態
    let manager = CBCentralManager()
    var foundDevices = [String]()
    
    override init() {
        super.init()
        manager.delegate = self
        manager.scanForPeripherals(withServices: nil, options: nil)
    }
}

extension BluetoothManager: CBCentralManagerDelegate {
    //當藍牙狀態更改時會更新
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        //設備掃描，CBCentralManagerScanOptionAllowDuplicatesKey：掃描設備訊號
        manager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
        
    }
    
    //藍牙控制中心，對外部設備進行搜尋、發現以及連接
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if let name = peripheral.name {
            if foundDevices.contains(name) == false {
                foundDevices.append(name)
                NotificationCenter.default.post(name: NSNotification.Name("didUpdate"), object: nil)
            }
            let max = 80.0
            let min = 40.0
            let normalRSSI = (abs(RSSI.doubleValue) - min) / (max - min)
            let signal = CGFloat(1 - round(normalRSSI * 10) / 10.0) * 100.0
            //如果> 100返回100，如果小於0，則返回0，否則返回信號值
            let formattedSignal = signal > 100.0 ? 100.0 : (signal < 0.0 ? 0.0 : signal)
            let signalData = [name: formattedSignal]
            NotificationCenter.default.post(name: NSNotification.Name("signal"), object: signalData)
            
        }
    }
}
