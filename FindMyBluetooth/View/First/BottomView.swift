//
//  FirstView.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit

class BottomView: UIView {
    
    let bluetoothBtn = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        self.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.bottom)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.width.equalTo(width)
            make.height.equalTo(height * 0.5)
        }
        self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.addSubview(bluetoothBtn)
        bluetoothBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(width * 0.3)
            make.height.equalTo(width * 0.3)
        }
        bluetoothBtn.setImage(UIImage(named: "bluetooth"), for: .normal)
    }
}
