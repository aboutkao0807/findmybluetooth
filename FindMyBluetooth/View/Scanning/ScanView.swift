//
//  ResultView.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit

class ScanView: UIView {
    
    let scanningLabel = UILabel()
    let remindLabel = UILabel()
    let buletoothImage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupUI() {
        self.addSubview(scanningLabel)
        scanningLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top).offset(width * 0.3)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.width.equalTo(width)
            make.height.equalTo(width * 0.3)
        }
        scanningLabel.text = "藍芽設備尋找中..."
        scanningLabel.textAlignment = .center
        scanningLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        scanningLabel.font = UIFont.systemFont(ofSize: width * 0.05)
        
        self.addSubview(remindLabel)
        remindLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.bottom.equalTo(self.snp.bottom).inset(width * 0.3)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.width.equalTo(width)
            make.height.equalTo(width * 0.3)
        }
        remindLabel.text = "請確保您的藍芽設備有開啟搜尋功能"
        remindLabel.textAlignment = .center
        remindLabel.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        remindLabel.font = UIFont.systemFont(ofSize: width * 0.04)
        
        self.addSubview(buletoothImage)
        buletoothImage.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(width * 0.3)
            make.height.equalTo(width * 0.3)
        }
        buletoothImage.image = UIImage(named: "bluetooth")
    }
}
