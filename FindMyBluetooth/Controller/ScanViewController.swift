//
//  SecViewController.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit

class ScanViewController: UIViewController {
    
    private let scanningView = ScanView()
    private let resultVC = ResultViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        setupUI()
        
        //第一次動畫
        animateLayer(layer: setupAnimationLayer())
        //第二次動畫
        animateLayer(layer: setupAnimationLayer(),delay: 0.3)
        
       
    }
   
    //過幾秒後顯示掃描完成
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadScan()
    }
    
    
    private func loadScan() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.navigationController?.pushViewController(self.resultVC, animated: true)
        }
    }
    
    private func setupUI() {
        self.view.addSubview(scanningView)
        scanningView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
    }
    
    private func animateLayer(layer: UIView, delay: TimeInterval = 0.0) {
        UIView.animate(withDuration: 1.5, delay: delay) {
            layer.transform = CGAffineTransform(scaleX: 100, y: 100)
            layer.center = self.view.center
            layer.alpha = 0.1
        } completion: { (_) in
            layer.transform = CGAffineTransform.identity
            layer.center = self.view.center
            layer.alpha = 1.0
            self.animateLayer(layer: layer, delay: delay)
        }
    }
    
    private func setupAnimationLayer() -> UIView {
        let animationLayer = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        animationLayer.backgroundColor = .link
        animationLayer.center = view.center
        animationLayer.layer.cornerRadius = animationLayer.frame.width / 2
        view.addSubview(animationLayer)
        view.sendSubviewToBack(animationLayer)
        return animationLayer
    }
    
    
    
}
