//
//  ResultView.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit

class ResultView: UIView {
    
    let titleLabel = UILabel()
    let scanBtn = UIButton()
    
    let tableView = UITableView()
    let selectedBackgroundView = UIView()
    
    let manager = BluetoothManager()


    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        self.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(width * 0.2)
            make.left.equalTo(self.snp.left).offset(width * 0.1)
            make.width.equalTo(width)
            make.height.equalTo(width * 0.2)
        }
        titleLabel.text = "搜尋結果："
        titleLabel.textAlignment = .left
        titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        titleLabel.font = UIFont.boldSystemFont(ofSize: width * 0.05)
        
        self.addSubview(scanBtn)
        scanBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.bottom).inset(width * 0.1)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(width * 0.8)
            make.height.equalTo(width * 0.1)
        }
        scanBtn.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        scanBtn.setTitle("重新搜尋", for: .normal)
        scanBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        scanBtn.layer.cornerRadius = width * 0.05
        
        self.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(width * 0.01)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.width.equalTo(width)
            make.height.equalTo(width * 1.5)
        }
        tableView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        tableView.register(ResultTViewCell.self, forCellReuseIdentifier: "ResultTViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
  
}

extension ResultView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return manager.foundDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTViewCell", for: indexPath) as! ResultTViewCell
        cell.layer.cornerRadius = width * 0.05
        cell.layer.borderWidth = width * 0.02
        cell.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.selectedBackgroundView = selectedBackgroundView
        
        cell.deviceNameLabel.text = "\(manager.foundDevices[indexPath.row])"

        cell.configure(name: cell.deviceNameLabel.text!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return width * 0.2
    }
    
    
}
