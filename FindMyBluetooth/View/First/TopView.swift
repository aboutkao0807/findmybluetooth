//
//  SecView.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit

class TopView: UIView {
    
    private let titleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        self.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.width.equalTo(width)
            make.height.equalTo(height * 0.5)
        }
        self.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        self.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(width)
            make.height.equalTo(width * 0.3)
        }
        titleLabel.text = "尋找我的藍芽設備"
        titleLabel.font = UIFont.boldSystemFont(ofSize: width * 0.05)
        titleLabel.textAlignment = .center
        titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
}
