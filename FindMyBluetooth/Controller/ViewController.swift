//
//  ViewController.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    let bottomView = BottomView()
    let topView = TopView()
    let scanVC = ScanViewController()
   

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        setupUI()
    }
    
    
    func setupUI() {
        self.view.addSubview(topView)
        topView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
        }
        
        
        self.view.addSubview(bottomView)
        bottomView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom)
        }
        bottomView.bluetoothBtn.addTarget(self, action: #selector(startScanningAction), for: .touchUpInside)
    }
    
    @objc func startScanningAction() {
        self.navigationController?.pushViewController(scanVC, animated: true)
    }
   
}

