//
//  ResultViewController.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit

class ResultViewController: UIViewController {

    private let resultView = ResultView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        NotificationCenter.default.addObserver(forName: NSNotification.Name("didUpdate"), object: nil, queue: nil) { (_) in
            self.resultView.tableView.reloadData()
        }
    }
    

    private func setupUI() {
        self.view.addSubview(resultView)
        resultView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        resultView.scanBtn.addTarget(self, action: #selector(scanAgainAction), for: .touchUpInside)
    }
    
    @objc func scanAgainAction() {
        self.navigationController?.popViewController(animated: true)
//        self.view = nil
    }

}

