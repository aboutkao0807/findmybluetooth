//
//  ResultTViewCell.swift
//  FindMyBluetooth
//
//  Created by Kao on 2020/9/20.
//

import UIKit

class ResultTViewCell: UITableViewCell {
    
    let deviceNameLabel = UILabel()
    let signalLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.addSubview(deviceNameLabel)
        deviceNameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.left).offset(width * 0.05)
            make.width.equalTo(width * 0.4)
            make.height.equalTo(width * 0.1)
        }
        deviceNameLabel.textAlignment = .left
        deviceNameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        deviceNameLabel.font = UIFont.systemFont(ofSize: width * 0.04)
//        deviceNameLabel.text = "我的設備"
        
        self.addSubview(signalLabel)
        signalLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.snp.centerY)
            make.right.equalTo(self.snp.right).inset(width * 0.05)
            make.width.equalTo(width * 0.15)
            make.height.equalTo(width * 0.1)
        }
        signalLabel.textAlignment = .center
        signalLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        signalLabel.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        signalLabel.font = UIFont.systemFont(ofSize: width * 0.03)
        signalLabel.layer.cornerRadius = width * 0.02
        signalLabel.layer.masksToBounds = true
//        batteryLabel.text = "100%"
        
     
    }
    
    func configure(name: String) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name("signal"), object: nil, queue: nil) { (notification) in
            self.deviceNameLabel.text = name
            if let value = (notification.object as? [String: CGFloat])?[name] {
                self.signalLabel.text = "\(String(format: "%.f", value))%"
            }
        }
    }
}
